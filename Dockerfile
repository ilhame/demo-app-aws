FROM golang:1.21-alpine3.18 as build

WORKDIR /demo

COPY . .

RUN go mod tidy
RUN go build -o /app main.go

FROM alpine:latest

COPY --from=build /app /myappp

ENTRYPOINT [ "/myappp" ]

